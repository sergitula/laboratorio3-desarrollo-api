from rest_framework import routers
from apps.producto import api

#Initializar el router de DRF solo una vez
router = routers.DefaultRouter()
# Registrar un ViewSet
router.register('producto',api.ProductoViewSet)
router.register('orden',api.OrdenViewSet)
router.register('detalle', api.DetalleOrdenViewSet)
