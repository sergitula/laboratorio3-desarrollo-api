import requests
from rest_framework import serializers, request
from rest_framework.exceptions import ValidationError

from .models import Producto, DetalleOrden, Orden


class ProductoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Producto
        fields = '__all__'

    def validate_stock(self, value):
        if value == 0:
            raise serializers.ValidationError('El Stock tiene que ser mayor que cero.')
        return value


class DetalleOrdenSerializer(serializers.ModelSerializer):
    class Meta:
        model = DetalleOrden
        fields = ['id','producto', 'cantidad']


class OrdenSerializer(serializers.ModelSerializer):
    detalles_orden = DetalleOrdenSerializer(many=True)
    total_orden = serializers.SerializerMethodField(method_name='get_total')
    total_orden_usd = serializers.SerializerMethodField(method_name='get_total_usd')

    class Meta:
        model= Orden
        fields = ['id',
                  'fecha_hora', 'detalles_orden', 'total_orden', 'total_orden_usd',]

    def get_total(self, orden):
        return orden.get_total_orden()


    def get_total_usd(self, orden):
        json = requests.get('https://www.dolarsi.com/api/api.php?type=valoresprincipales').json()
        dolar_blue_compra = json[1]['casa']['compra'].replace(',', '.')
        cotizar_dolar = float(orden.get_total_orden()) / float(dolar_blue_compra)
        return str(round(cotizar_dolar, 2)) + ' USD'


    def create(self, validated_data):
        detalles_data = validated_data.pop('detalles_orden')
        orden = Orden.objects.create(**validated_data)
        listaVerificiacion = []
        for detalle_data in detalles_data:
            #Controla que no haya repetidos
            if detalle_data['producto'].id in listaVerificiacion:
                orden.delete()
                raise ValidationError('El producto "' + detalle_data['producto'].nombre + '" ya se encuentra en el detalle, no se admiten repetidos.')
            else:
                listaVerificiacion.append(detalle_data['producto'].id)

            # Controla que no haya articulos comprados en cantidad 0
            if detalle_data['cantidad'] == 0:
                orden.delete()
                raise ValidationError('El producto "' + detalle_data['producto'].nombre + '" se registró en cantidad 0, es invalido')
            # producto = Producto.objects.get(id=detalle_data['producto'])
            #print(detalle_data['producto'])

            # Controla que la cantidad ordenada no supere el stock
            if detalle_data['producto'].stock < detalle_data['cantidad']:
                orden.delete()
                raise ValidationError('El producto "' + detalle_data['producto'].nombre +'" no tiene suficiente stock.' )

            #Actualiza el stock del producto
            productoV = Producto.objects.get(id=detalle_data['producto'].id)
            productoV.stock = productoV.stock - detalle_data['cantidad']
            productoV.save()
            DetalleOrden.objects.create(orden=orden, **detalle_data)
        return orden


    # UPDATE NO ES NECESARIO REALIZARLO
    # def update(self, instance, validated_data):
    #     detalles_data = validated_data.pop('detalles_orden')
    #     print(detalles_data)
    #
    #    # instance.id = validated_data.get('id', instance.id)
    #    # print(instance)
    #    # print(instance.id)
    #
    #     instance.save()
    #     # obtener lista de id detalles con la misma instance de orden
    #     #Consultado en https://blog.devgenius.io/nested-serializers-in-django-rest-framework-6b36bf011074
    #     detalles_con_misma_instance_orden = DetalleOrden.objects.filter(orden=instance.pk).values_list('id', flat=True)
    #     print(detalles_con_misma_instance_orden)
    #     detalles_id_pool = []
    #
    #     # muchos detalles
    #     for detalle_data in detalles_data:
    #         if "id" in detalle_data:
    #             if DetalleOrden.objects.filter(id=detalle_data['id']).exists():
    #                 detalle_instance = DetalleOrden.objects.get(id=detalle_data['id'])
    #                 detalle_instance.producto = detalle_data.get('producto', detalle_instance.producto)
    #                 detalle_instance.cantidad = detalle_data.get('cantidad', detalle_instance.cantidad)
    #                 detalle_instance.save()
    #                 detalles_id_pool.append(detalle_instance.id)
    #             else:
    #                 continue
    #         else:
    #             detalles_instance = DetalleOrden.objects.create(orden=instance, **detalle_data)
    #             detalles_id_pool.append(detalles_instance.id)
    #
    #     for detalle_id in detalles_con_misma_instance_orden:
    #         if detalle_id not in detalles_id_pool:
    #             print("detalle_id:", detalle_id)
    #             detalleElim = DetalleOrden.objects.get(id=detalle_id)
    #     #        print(type(detalleElim))
    #             #print(detalleElim['producto'])
    #             #detalleElim = DetalleOrden.objects.filter(pk=detalle_id)
    #     #        productoV = Producto.objects.get(id=(detalleElim['producto'].id))
    #     #        productoV.stock = productoV.stock + detalleElim['cantidad']
    #     #        productoV.save()
    #             detalleElim.delete()
    #     return instance




