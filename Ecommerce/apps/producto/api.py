from rest_framework import viewsets
from rest_framework.response import Response

from .models import Producto, Orden, DetalleOrden
from .serializers import ProductoSerializer, OrdenSerializer, DetalleOrdenSerializer


class ProductoViewSet(viewsets.ModelViewSet):
    queryset = Producto.objects.all()
    serializer_class = ProductoSerializer


class OrdenViewSet(viewsets.ModelViewSet):
    queryset = Orden.objects.all()
    serializer_class = OrdenSerializer

    def perform_destroy(self, orden):
        # Recorro los datos del detalle y devuelvo el producto al stock
        for detalle in orden.detalles_orden.all():
            producto_orden = detalle.producto
            producto_orden.stock = producto_orden.stock + detalle.cantidad
            producto_orden.save()
        # Finalmente Elimino la orden
        orden.delete()

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        detalles_instancia = instance.detalles_orden.all()

        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)

    #def perform_update(self, serializer):
    #    serializer.save()



class DetalleOrdenViewSet(viewsets.ModelViewSet):
    queryset = DetalleOrden.objects.all()
    serializer_class = DetalleOrdenSerializer



