import pytest
#import self
from rest_framework import status
from rest_framework.exceptions import ValidationError
from .fixtures import crear_productos, crear_orden, api_client, get_default_test_user
from ..models import DetalleOrden, Producto, Orden


"""1. Verificar que al ejecutar el endpoint de recuperación de una orden,
 se devuelven los datos correctos de la orden y su detalle."""

@pytest.mark.django_db
def test_api_recuperacion_orden(api_client,crear_orden,crear_productos, get_default_test_user):
    client = api_client
    client.force_authenticate(user=get_default_test_user)
    orden1 = crear_orden
    id_orden1= orden1.id
    cantidad_detalles_orden1= (orden1.detalles_orden).count()
    producto1, producto2 = crear_productos
    ordenes = client.get('/orden/')
    json_data = ordenes.json()
    # Verifico si devuelve la orden correcta
    assert json_data[0]['id'] is not None
    assert json_data[0]['id'] == id_orden1
    # Verifico si devuelve el detalle correcto
    assert DetalleOrden.objects.filter(orden__id=json_data[0]['id']).count() == cantidad_detalles_orden1
    assert json_data[0]['detalles_orden'][0]['producto'] == producto1.id
    assert json_data[0]['detalles_orden'][1]['producto'] == producto2.id


"""2. Verificar que al ejecutar el endpoint de creación de una orden, ésta
 se cree correctamente junto con su detalle, y que además, se haga actualizado
  el stock de producto relacionado con un detalle de orden. Se debe considerar aquí,
   que los datos de la orden a crear, no posea productos repetidos y que la cantidad
    de productos en el detalle de la orden, sea menor o igual al stock del producto."""

@pytest.mark.django_db
def test_api_creacion_orden(api_client, get_default_test_user, crear_productos):
    client = api_client
    client.force_authenticate(user=get_default_test_user)
    producto1, producto2 = crear_productos
    stock_producto1 = producto1.stock
    stock_producto2 = producto2.stock
    data = { #Considero productos no repetidos y cantidad menor al stock total
        "detalles_orden": [
             {"producto": producto1.id, "cantidad": "2"},
             {"producto": producto2.id, "cantidad": "3"}
        ]
    }
    orden_creada = client.post('/orden/', data, format='json')
    json_data = orden_creada.json()
    # Verifico la correcta creación de la orden
    assert orden_creada.status_code == status.HTTP_201_CREATED
    # Verifico la cantidad y los productos incluidos
    assert json_data['id'] is not None
    assert json_data['detalles_orden'][0]['producto'] == producto1.id
    assert DetalleOrden.objects.filter(producto = producto1, cantidad = 2, orden__id= json_data['id']).count() == 1
    assert json_data['detalles_orden'][1]['producto'] == producto2.id
    assert DetalleOrden.objects.filter(producto=producto2, cantidad=3, orden__id=json_data['id']).count() == 1
    # Verifico que se halla actualizado el stock correspondiente
    producto1.refresh_from_db(fields=['stock'])
    assert producto1.stock == stock_producto1 - 2
    producto2.refresh_from_db(fields=['stock'])
    assert producto2.stock == stock_producto2 - 3

    # Método 1--> Accede a la BD y recupera el stock
    # assert Producto.objects.get(pk = producto1.id).stock == stock_producto1 - 1


"""3. Verificar que al ejecutar el endpoint de creación de una orden, se produzca 
un fallo al intentar crear una orden cuyo detalle tenga productos repetidos."""


@pytest.mark.django_db
def test_api_creacion_orden_detalles_repetidos(api_client, get_default_test_user, crear_productos):
    client = api_client
    client.force_authenticate(user=get_default_test_user)
    producto1, producto2 = crear_productos
    data = {
        "detalles_orden": [
            {"producto": producto1.id, "cantidad": "1"},
            {"producto": producto1.id, "cantidad": "3"}
        ]
    }
    orden_creada = client.post('/orden/', data, format='json')
    json_data = orden_creada.json()
    # Verifico que la orden no se halla creado
    assert orden_creada.status_code == status.HTTP_400_BAD_REQUEST
    # Verifico que la orden y el detalle no esten contenidos en la BD
    assert Orden.objects.all().count() == 0
    assert DetalleOrden.objects.all().count() == 0


"""4. Verificar que al ejecutar el endpoint de creación de una orden, se
 produzca un fallo al intentar crear una orden donde la cantidad de un producto
  del detalle, sea mayor al stock de ese producto."""


@pytest.mark.django_db
def test_api_creacion_orden_cantidad_supera_stock(api_client, get_default_test_user, crear_productos):
    client = api_client
    client.force_authenticate(user=get_default_test_user)
    producto1, producto2 = crear_productos
    data = {
        "detalles_orden": [
            {"producto": producto1.id, "cantidad": (producto1.stock + 4)},  #Stock max producto1 = 10
            {"producto": producto2.id, "cantidad": "3"}   #Stock max producto2 = 20
        ]
    }
    orden_creada = client.post('/orden/', data, format='json')
    json_data = orden_creada.json()
    # Verifico que la orden no se halla creado
    assert orden_creada.status_code == status.HTTP_400_BAD_REQUEST
    # Verifico que la orden y el detalle no esten contenidos en la BD
    assert Orden.objects.all().count() == 0
    assert DetalleOrden.objects.all().count() == 0


"""5. Verificar que al ejecutar el endpoint de eliminación de una orden, ésta 
se haya eliminado de la base de datos correctamente, junto con su detalle, y que 
además, se haga incrementado el stock de producto relacionado con cada detalle de orden."""

@pytest.mark.django_db
def test_api_eliminacion_orden(api_client,crear_orden,get_default_test_user):
    client = api_client
    client.force_authenticate(user=get_default_test_user)
    orden = crear_orden
    id_orden = orden.id
    detalles = orden.detalles_orden.all()
    cantidad_detalle1 = detalles[0].cantidad
    cantidad_detalle2 = detalles[1].cantidad
    producto1 = detalles[0].producto
    stock_anterior1 = producto1.stock
    producto2 = detalles[1].producto
    stock_anterior2 = producto2.stock
    # Consulto los elementos de la BD antes de eliminar
    assert Orden.objects.filter(id=id_orden).count() == 1
    # Planteamos y verificamos el método de eliminación
    orden_eliminada = client.delete("/orden/{}/".format(id_orden))
    assert orden_eliminada.status_code == status.HTTP_204_NO_CONTENT
    # Verificando que que la orden creada se elimino, junto con sus detalles
    assert Orden.objects.filter(id=id_orden).count() == 0
    assert DetalleOrden.objects.filter(orden__id=id_orden).count() == 0
    # Verificar incremento del stock
    # cantidad_producto1 = detalles[1].cantidad
    assert Producto.objects.get(id=producto1.id).stock == stock_anterior1 + cantidad_detalle1  #Cantidad = 3
    assert Producto.objects.get(id=producto2.id).stock == stock_anterior2 + cantidad_detalle2  #Cantidad = 2


"""7. Verificar que el método get_total de una orden, devuelve el valor 
correcto de acuerdo al total de cada detalle."""

@pytest.mark.django_db
def test_get_total_orden(crear_orden):
    orden = crear_orden
    detalles = orden.detalles_orden.all()
    cantidad_producto1 = detalles[0].cantidad
    cantidad_producto2 = detalles[1].cantidad
    producto1 = detalles[0].producto
    precio_producto1 = producto1.precio
    producto2 = detalles[1].producto
    precio_producto2 = producto2.precio
    total_esperado = precio_producto1 * cantidad_producto1 + precio_producto2 * cantidad_producto2 #Get_total = 748
    total_actual = Orden.get_total_orden(orden)
    assert total_actual == total_esperado


"""8. Verificar que el método get_total_detalle de un detalle de orden,
 devuelve el valor correcto de acuerdo a al precio del producto y cantidad de la orden."""


@pytest.mark.django_db
def test_get_total_detalle(crear_orden):
    orden = crear_orden
    detalles = orden.detalles_orden.all()
    producto1 = detalles[0].producto
    precio_producto1 = producto1.precio
    producto2 = detalles[1].producto
    precio_producto2 = producto2.precio
    # Tomo el detalle 1
    total_detalle1_esperado = precio_producto1 * detalles[0].cantidad  # Total_detalle = 558
    total_detalle1 = DetalleOrden.get_total_detalle(detalles[0])
    # Tomo el detalle 2
    total_detalle2_esperado = precio_producto2 * detalles[1].cantidad  # Total_detalle = 190
    total_detalle2 = DetalleOrden.get_total_detalle(detalles[1])
    assert total_detalle1 == total_detalle1_esperado
    assert total_detalle2 == total_detalle2_esperado