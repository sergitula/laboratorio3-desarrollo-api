import pytest
from rest_framework import status
from django.test.client import encode_multipart
from .fixtures import crear_productos, api_client, get_default_test_user, crear_producto
# from apps.producto.tests.fixtures import api_client, get_default_test_user
from ..models import Producto



@pytest.mark.django_db
def test_api_usuario_logueado(api_client, get_default_test_user):
    client = api_client
    client.force_authenticate(user = get_default_test_user)
    response = client.get('/producto/', logging=get_default_test_user)
    assert response.status_code == status.HTTP_200_OK



"""6. Verificar que al ejecutar el endpoint de modificación del stock de
 un producto, actualiza correctamente dicho stock."""

# @pytest.mark.django_db
# def test_api_modificacion_producto(api_client, crear_producto):
#     client = api_client
#     client.force_authenticate(user=get_default_test_user)
#     producto = crear_producto
#     #producto_refreso = crear_producto
#     data = {
#         'nombre': 'Test_Producto1',
#         'precio': 170,
#         'stock': 15
#     }
#     edicion = client.put('/producto/{1}', data, format='json')
#     #producto_refreso.refresh_from_db()
#     producto_refresco = Producto.objects.get(pk=1)
#     assert producto_refresco.stock == (producto.stock + 5)
#




# """6. Verificar que al ejecutar el endpoint de modificación del stock de
#  un producto, actualiza correctamente dicho stock.
#     VERSION ANTES DEL MERGE (MAIN) - REVISAR"""
# @pytest.mark.django_db
# def test_api_modificacion_producto(api_client, crear_producto):
#     client = api_client
#     client.force_authenticate(user=get_default_test_user)
#     producto = crear_producto
#     id_producto = producto.id
#     data = {
#         "id": id_producto,
#         "stock": 12
#     }
#
#     edicion = client.patch('/producto/{}'.format(id_producto),data,content_type = 'application/json')
#     assert edicion.status_code == status.HTTP_301_MOVED_PERMANENTLY
#     assert Producto.objects.get(pk=id_producto).stock == 12
