from django.db import models

# Create your models here.

class Producto(models.Model):
    nombre = models.CharField(max_length=250)
    precio = models.DecimalField(max_digits=8, decimal_places=2)
    stock = models.IntegerField()

    def __str__(self):
        return self.nombre

    def __float__(self):
        return self.stock



class Orden(models.Model):
    fecha_hora = models.DateTimeField(auto_now_add=True)

    def get_total_orden(self):
        total_orden = 0
        for detalle in self.detalles_orden.all():
            total_orden = total_orden + detalle.get_total_detalle()
        return total_orden
        # Accede a detalles_orden a partir de su "relacion inversa", es decir, a partir del seft



class DetalleOrden(models.Model):
    orden = models.ForeignKey(Orden, on_delete=models.CASCADE, related_name='detalles_orden')
    cantidad = models.IntegerField()
    producto = models.ForeignKey(Producto, on_delete=models.CASCADE)
    precio_unitario = models.DecimalField(max_digits=8, decimal_places=2, null=True)

    # Al momento de crear el detalle guardamos el precio_unitario del producto seleccionado
    def save(self, *args, **kwargs):
        self.precio_unitario = self.producto.__getattribute__('precio')
        super(DetalleOrden, self).save(*args, **kwargs)

    def get_total_detalle(self):
        precio_total_detalle = self.precio_unitario * self.cantidad
        return precio_total_detalle
